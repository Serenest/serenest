#!/usr/bin/env python3

#python -m setup bdist_wheel

import pathlib
from setuptools import setup

pwd = pathlib.Path(__file__).parent

README = (pwd / "README.md").read_text()

setup(
    name="serenest",
    version="0.2.0",
    description="cryptography tools for CTF, by Serenest",
    long_description=README,
    url = "https://gitlab.com/Serenest/serenest",
    author="Serenest",
    author_email="serenest.ctf@gmail.com",
    license="MIT",
    classifiers=[
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 3",
    ],
    packages=["serenest"],
    install_requires=[],#add stuff here

)

