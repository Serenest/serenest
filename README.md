This library is still work in progress.

Before reaching version 1.0.0, I plan on adding at least the following features:

Cryptoutils:
    -Tonelli-Shanks
    -AKS primality test

Diffie-Hellman and DLP:
    -pohlig-hellman

Factoring:
    -pollard
    -difference of squares
    -number field sieve

Various:
    -Cleaner documentation



